#|
    Exercise 1.2: Translate the following expression into prefix
form:
    5 + 4 + (2 - (3 - (6 + 4/5)))
        /
        3(6 - 2)(2 - 7)
|#

(define ans1-2 
 (/ (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5))))) (* 3 (- 6 2) (- 2 7))))

#| 
    Exercise 1.3: Define a procedure that takes three numbers
    as arguments and returns the sum of the squares of the two
    larger numbers.
|#

(define (ans1-3 x y z)
    (cond ((= x (max x y z)) 
           (+ (square x) (square (max y z))))
          ((= y (max x y z)) 
           (+ (square y) (square (max x z))))
          (else (+ (square z) (square (max x y))))))

#|
    Exercise 1.4: Observe that our model of evaluation allows
    for combinations whose operators are compound expressions.
    Use this observation to describe the behavior of the
    following procedure:

    (define (a-plus-abs-b a b)
    ((if (> b 0) + -) a b))

    Answer: It's a function which calculates `a + |b|` by 
    evaluating the operator based on the value of `b` and after
    that it executes (+|- a b).
---------------------------------------------------------------
    Exercise 1.5: Ben Bitdiddle has invented a test to determine
    whether the interpreter he is faced with is using applicative-
    order evaluation or normal-order evaluation. He defines the
    following two procedures:

    (define (p) (p))
    (define (test x y)
      (if (= x 0) 0 y))

    Then he evaluates the expression

    (test 0 (p))

    What behavior will Ben observe with an interpreter that
    uses applicative-order evaluation? What behavior will he
    observe with an interpreter that uses normal-order evalu-
    ation? Explain your answer.

    Nomal-order = "fully expand then reduce"
    Applicative-order = "Evaluate the arguments and then apply""

    Answer: TODO
|#
